package s03;
    /*Написать программу, которая выводит последовательность рандомных целых чисел (ПИН-код)
    по количеству регистров, которые вводятся из консоли. Например, N=7 (N-вводится из консоли).
    Результат: 5412409.
    */

import java.util.Random;
import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        int n = requestNumber();
        int dioapazon = 10;
        Random r = new Random();
        for (int i = 0; i < n; i++) {
            int x = r.nextInt(dioapazon);
            System.out.print(x);
        }
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
