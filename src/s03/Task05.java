package s03;
/*Из консоли вводится строка. Определить является ли строка палиндромом (например: мадам, комок, ротор).*/

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner stroka = new Scanner(System.in);
        System.out.print("Введите строку: ");
        String s = stroka.nextLine();
        s = s.toLowerCase(); //Перевод строки в один регистр символов если будет введен разный размер символов
        String[] arra = s.split(" "); // В массив строк arra добавляются слова строки разделенные пробелом
        for (String array1 : arra) { // Цикл форич  в array1 присваивается слово
            System.out.println(array1); // Выводится присвоенное слово
            char[] array = array1.toCharArray(); //разбивка строки на символы и помещение в массив символов
            StringBuilder result = new StringBuilder(); //обьявление пустой строкм result

            for (int i = array.length - 1; i >= 0; i--) { //переворачивание слова и запись в resolt
                result.append(array[i]);
            }
            System.out.println("Переыернутое слово  " + result); // Вывод перевернутого слова
            if (array1.equals(result.toString())) {
                System.out.println("Слово является палиндромом");
            } else {
                System.out.println("Слово не является палиндромом");
            }
        }
    }
}


