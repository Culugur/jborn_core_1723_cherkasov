package s03;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {

        Scanner gosudarstvo = new Scanner(System.in);
        System.out.print("Введите государство: ");
        String strana = gosudarstvo.nextLine();

        Scanner stolica = new Scanner(System.in);
        System.out.print("Введите его столицу: ");
        String gorod = stolica.nextLine();


        System.out.println("Столица государства  " + strana + " - город " + gorod);
    }

}
