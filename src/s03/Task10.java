package s03;

import java.util.Scanner;

/*Из консоли вводится предложение. Определить ошибся ли автор в написании "Жи/Ши пиши с буквой и".*/
public class Task10 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        String str = scanner.nextLine();

        String zhiWrong = "жы";
        String shiWrong = "шы";
        String zhiRight = "жи";
        String shiRight = "ши";

        str = str.toLowerCase();

        if (str.contains(zhiRight) || str.contains(shiRight)) {
            System.out.println("Правильно!");
        } else if (str.contains(zhiWrong) || str.contains(shiWrong)) {
            System.out.println("Неправильно! Жи/Ши пиши с буквой 'и'");
        } else
            System.out.println("Для этого слова правило не применяется");
    }
}