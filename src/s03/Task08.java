package s03;
/*Инверсия строки. Из консоли вводится строка, реализовать ее инверсию.
Не использовать метед reverse(). Например:
 Строка: "Привет всем моим друзьям!"
 Результат инверсии: "!мяьзурд миом месв тевирП"*/

import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите слово или набор слов");
        String str = in.nextLine();
        StringBuilder res = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            res.insert(0, str.charAt(i));
        }
        System.out.println(res);
    }
}
