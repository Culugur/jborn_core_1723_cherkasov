package s03;

import javax.xml.transform.SourceLocator;
import java.util.Scanner;

/*Вывести в консоле строки, состоящие из введенных символов.
Строка вводится с клавиатуры. Например, слово «колбаса». Результат:
к
оо
ллл
бббб
ааааа
сссссс
ааааааа*/
public class Task03 {
    public static void main(String[] args) {
        Scanner stroka = new Scanner(System.in);
        System.out.print("Введите строку: ");
        String str1 = stroka.nextLine();
        char n;
        for (int i = 0; i < str1.length(); i++) {
             for (int j = 0; j < i + 1; j++) {
                    n = str1.charAt(i);
                    System.out.print(n);
                }
            System.out.println();
            }
        }
    }


