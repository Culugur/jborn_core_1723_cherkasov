package s03;

import java.util.Scanner;
import java.util.StringJoiner;

/*Из консоли вводится массив строк (вспомним как в первом занятии вводили массивы,
сначала размер, потом по элементу),
 вывести все введенные строки через запятую.
 Учесть что на краях новой строки не должно быть запятых. Например:
        Введите кол-во строк: 3
        Строка 0: красный
        Строка 1: синий
        Строка 2: жельтый
        Результат: красный, синий, жельтый*/
public class Task09 {
    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        System.out.println("Введите размер массива");
        int razmer = str.nextInt();
        String[] arr = new String[razmer];
        StringJoiner str1 = new StringJoiner(", ");
        for (int i = 0; i < razmer; i++) {
            Scanner st = new Scanner(System.in);
            System.out.println("Введите строку: " + i);
            arr[i] = st.nextLine();
            str1.add(arr[i]);
        }
        System.out.println(str1);
    }
}
