package s03;
/*Нарисовать в консоли прямоугольник с 2-мя заданными сторонами. Стороны (целые числа) запрашиваются из консоли A и B.
 Например A=10 и B=5, то вывести:
 */
/*
 **********
 *          *
 *          *
 *          *
 **********

 */

import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Ширина:  ");
        int n = in.nextInt();

        System.out.println("Высота ");
        int m = in.nextInt();

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 || i == m - 1 || j == 0 || j == n - 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}