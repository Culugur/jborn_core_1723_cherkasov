package s03;
/*Из консоли вводится строка и символ.
Определить сколько раз встречается данный символ и заменить в строке этот символ на верхний регистр,
например, str="java" и ch='a', то результат:
        Кол-во вхождений: 2
        Преобразованная строка: jAvA*/

import java.io.IOException;
import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) throws IOException {
        char simvol, reg, n;
        int sum = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("Введите слово:  ");
        String str = in.nextLine();
        str = str.toLowerCase();

        System.out.println("Введите сивол: ");
        String ch = in.nextLine();
        ch = ch.toLowerCase();// Получение данных из консоли и изменение в нижний регистр
        simvol = ch.charAt(0); //Присвоение символьной переменной символа из строки полученной из консоли

        for (int j = 0; j < str.length(); j++) {
            n = str.charAt(j); //Присвоение переменной n символа строки. при каждой итеррации
            if (n == simvol) {
                sum += 1;
            }
        }
        reg = Character.toUpperCase(simvol);// Присвоение переменной l нужного символа с измененном регистром
        System.out.println("Количество вхождений: " + sum);
        System.out.println(str.replace(simvol, reg));
    }
}

