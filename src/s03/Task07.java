package s03;

import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        Scanner stroka = new Scanner(System.in);
        System.out.print("Введите строку: ");
        String str = stroka.nextLine();
        char h;
        char[] numbers = new char[str.length()];

        for (int i = 0; i < str.length(); i++) {
            numbers[i] = str.charAt(i);
            if (Character.isUpperCase(numbers[i])) {
                numbers[i] = Character.toLowerCase(numbers[i]);
            } else {
                if (Character.isLowerCase(numbers[i])) {
                    numbers[i] = Character.toUpperCase(numbers[i]);
                }
            }
            System.out.print(" " + numbers[i]);
        }
    }
}
