package s02;

import java.util.Scanner;

public class Task4 {
    /*Известны координаты
двух точек (x1, y1) и (x2, y2) -
вводятся с клавиатуры. Составить программу
вычисления расстояния между ними.*/

    public static void main(String[] args) {

        double x1 = requestNumber();
        double y1 = requestNumber();
        double x2 = requestNumber();
        double y2 = requestNumber();
        double m, m2, n1, n2;
        int a = 2;
        m = (x2 - x1);
        m2 = (y2 - y1);
        n1 = Math.pow(m, a);
        n2 = Math.pow(m2, a);
        m = n1 + n2;
        m = Math.sqrt(m);

        System.out.println(m + " - расстояние между точками");

    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите координаты точек сначало первой х и у, затем второй:");
        return scanner.nextInt();
    }
}