package s02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import static java.util.Arrays.*;

public class Task11 {//*** В массиве хранятся сведения о количестве осадков

    // выпавших за каждый день в месяце (30 дней).
    // Определить в какие дни было максимальное и минимальное количество осадков
    // и вывести их номера в месяце и значения осадков.
    // Массив можно не считывать с клавиатуры, а инициализировать в программе.
    public static void main(String[] args) {

        int[] osadki = new int[]{16, 15, 10, 16, 25, 110, 45,
                81, 80, 11, 1, 89, 12, 13,
                14, 18, 19, 1, 1, 156, 59,
                78, 45, 69, 65, 24, 54, 156,
                63, 52};
        int[] numbersCopy = Arrays.copyOf(osadki, osadki.length);
        System.out.println(Arrays.toString(numbersCopy));
        int min;
        int max;
        min = max = osadki[0];

        for (int i = 0; i < osadki.length; i++) {
            if (osadki[i] < min)
                min = osadki[i];
            if (osadki[i] > max)
                max = osadki[i];

        }
        System.out.print("Максимальное количество осадков:  " + max + "  мм выпало: ");
        for (int i = 0; i < osadki.length; i++) {
            if (osadki[i] == max)
                System.out.print((i + 1) + ", ");
        }
        System.out.println(" числа");
        System.out.print("Минимальное количество осадков:  " + min + "  мм выпало: ");
        for (int i = 0; i < osadki.length; i++) {
            if (osadki[i] == min)
                System.out.print((i + 1) + ", ");
        }
        System.out.println(" числа");
    }
}