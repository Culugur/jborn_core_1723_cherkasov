package s02;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {

        int n = requestNumber();
        int m, b;
        if (n > 10 && n < 100) {
            b = n / 10;
            System.out.println(b + " - Количество десятков в числе.");
            m = n % 10;
            System.out.println(m + " - Количество едениц в числе.");
            m = m * b;
            System.out.println(m + " - Произведение цифр");
            m = m + b;
            System.out.println(m + " - сумма цифр");
        } else {
            System.out.println("Вы ввели число вне ниапазона, введите число от 10 до 99");
        }

    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите двухзначное число:");
        return scanner.nextInt();
    }
}
