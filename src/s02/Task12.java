package s02;

import java.util.Scanner;
/*Написать калькулятор «Расход денег на топливо для автомобиля».
 Запросить в консоли: расстояние пути в км, средний расход топлива на 100 км,
 стоимость одного литра топлива и вывести результат –
 расход топлива (в литрах) и денег (в рублях).*/

public class Task12 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Введите количество километров:  ");
        float s = in.nextInt();

        System.out.println("Введите средний расход на 100 км: ");
        float rasxod = in.nextFloat();

        System.out.println("Введите стоимость одного литра: ");

        float rub = in.nextFloat();
        float rasxodTop = (s / 100) * rasxod;
        float stoimost = rasxodTop * rub;
        System.out.println("На расстояние в: " + s + " километров ушло в литрах: " + rasxodTop +
                " И потрачено в рублях: " + stoimost);

    }
}

