package s02;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        int m = requestNumber();

        for (int i = 1; i <= 10; i++) {
            int j = m * i;

            System.out.print(i + " * " + m + " = ");
            System.out.println(j);
        }
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();


    }
}
