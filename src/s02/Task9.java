package s02;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        int n = requestNumber();
        int s = 0;
        int sum = 0;

        while (n > 0) {
            s = s + n % 10;
            n = n / 10;
        }

        System.out.println(s);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}