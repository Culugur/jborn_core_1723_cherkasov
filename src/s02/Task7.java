package s02;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        double x = requestNumber();
        double y;
        if (x > 0) {
            y = Math.sin (x) * Math.sin(x);
        } else {
            y = 1 - 2 * Math.sin(x * x);

        }
        System.out.println(y);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}