package s02;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        int n = requestNumber();
        int m;
        m = n / 3600;
        System.out.println("С начала суток прошло - " + m + " Часа");

        m = (n % 3600) / 60;
        System.out.println("С начала очередного часа - " + m + " минут");

        m = n % 60;
        System.out.println("С начала Очередной минуты прошло - " + m + " секунд");
    }


    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число в диапазоне от 1 до 86400 ");
        return scanner.nextInt();
    }
}

