package s02;

import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Math.pow;

public class Task5 {
    public static void main(String[] args) {
        // создаем массив, чтобы удобнее потом отсортировать
        int[] numbers = new int[3];
        numbers[0] = requestNumber();
        numbers[1] = requestNumber();
        numbers[2] = requestNumber();

        // с начала нужно отсортировать эти числа (x, y, z)10

        // в порядке возрастания
        Arrays.sort(numbers);

        // теперь проверяем являются ли эти числа тройкой Пифагора
        // по формуле: x^2 + y^2 = z^2
        // и выводим ответ
        if ((pow(numbers[0], 2) + pow(numbers[1], 2)) == pow(numbers[2], 2)) {
            System.out.println("Эти числа являются тройкой Пифагора");
        } else {
            System.out.println("Эти числа не являются тройкой Пифагора");
        }
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}