package s02;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        int n = requestNumber();
        int i, summ = 0;
        int proizvedenie = 1;

        int[] array = new int[n];

        for (i = 0; i < array.length; i++) {
            array[i] = requestNumber();  //заполнение массива


            //суммированние четных элементов
            if (array[i] % 2 == 0)
                summ += array[i];

                // произведение нечетных элементов
            else
                proizvedenie *= array[i];

            System.out.println("Элeмeнт[" + i + "]: " + array[i]);
        }

        System.out.println("Результ суммирования всех четных элементов  " + summ);
        System.out.println("Результ умножения всех нечетных элементов  " + proizvedenie);
    }


    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();


    }
}
