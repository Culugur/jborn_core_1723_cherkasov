package s02;
/*С клавиатуры вводятся натуральные числа X и Y.
Вычислить произведение x y, используя лишь операцию сложения.
 */

import java.util.Scanner;

public class Task15 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);//Используем клосс Skanner который использует объект System.in
        System.out.println("Необходимо ввести натуральное число " +
                "(Натуральным числом не может быть ноль и любые отрицательные цисла):  ");
        System.out.println("Введите x:  ");
        int x = in.nextInt();// принимает и обрабатывает целочисленные переменные
        if (x <= 0) {
            System.err.println("Ошибка (Введите положительное число больше ноля)  ");
            return;
        }
        System.out.println("Введите y:  ");
        int y = in.nextInt();
        if (y <= 0) {
            System.err.println("Ошибка (Введите положительное число больше ноля)  ");
            return;
        }
        int n = 0;
        for (int i = 0; i < y; i++) {
            n = n + x;
        }
        System.out.println(n);
    }


}