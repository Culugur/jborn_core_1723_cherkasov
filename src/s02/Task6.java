package s02;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        int n = requestNumber();
        double m;

        m = Math.PI * (n * n);
        System.out.println("Площадь круга равна " + m);
        m = 2 * Math.PI * n;
        System.out.println("Длинна окружности равна  " + m);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длинну радиуса:");
        return scanner.nextInt();
    }
}

