package s02;
/*  Найти сумму 2^0 + 2^1 + 2^2 + ... + 2^N (N - считывается из консоли).
Операцию возведения в степень не использовать. */

import java.util.Scanner;

public class Task14 {
    public static void main(String[] args) {
        int two = 2;
        int u = 1;
        int sum = 1;
        int d = requestNumber();
        for (int i = 0; i < d; i++) {
            u = two * u;
            sum +=  u;
        }

        System.out.println("Сумма всех чисел возведенных в степень:  " + sum);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}